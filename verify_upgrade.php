<?php
include("dbinfo.inc.php");
echo "
<div style='text-align:center;'>
	<h1>Perfect Books - Verify Upgrades</h1>
</div>
";

$con=mysqli_connect($servHost, $servUser, $servPass, $database);
// Check connection
if (mysqli_connect_errno()){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$result = mysqli_query($con,"SELECT * FROM {$table} ORDER BY ID DESC");

echo "
<style>
	.t{
		padding-left: 10px; 
		padding-right: 10px;
	}
</style>
<div style='width:100%;'>
<table border='1' style='margin: 0 auto; text-align:center;'>
<tr>
<th> ID </th>
<th> Licence Number </th>
<th> Version </th>
<th> VAT Status </th>
<th> Numbering Format </th>
<th> Max Users </th>
<th> Menu Style </th>
<th> Report Style </th>
<th> 64 Bit Backup </th>
<th> Date </th>
<th> Time </th>
</tr>";

while($row = mysqli_fetch_array($result)){
	echo "<tr>";
	echo "<td class=t>" . $row['ID'] . "</td>";
	echo "<td>" . $row['LicenceNumber'] . "</td>";
	echo "<td class=t>" . $row['Version'] . "</td>";
	echo "<td>" . $row['VATStatus'] . "</td>";
	echo "<td>" . $row['NumberingFormat'] . "</td>";
	echo "<td>" . $row['MaxUsers'] . "</td>";
	echo "<td>" . $row['MenuStyle'] . "</td>";
	echo "<td class=t>" . $row['ReportStyle'] . "</td>";	
	if($row['64BitBackup'] == 0){
		echo "<td>False</td>";
	}else{
		echo "<td>True</td>";
	}
	echo "<td class=t>" . date('d-m-Y', strtotime( $row['Date'])) . "</td>";
	echo "<td class=t>" . date('H:i:s', strtotime( $row['Date'])) . "</td>";
	echo "</tr>";
}
echo "</table></div>";

mysqli_close($con);

echo "
<div style='text-align:center;'>
	<p>Copyright Perfect Software 2014 - Version 1.0.1</p>
</div>
";

?>